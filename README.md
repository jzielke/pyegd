# pyEGD #

Python Entropy Gathering Daemon.  In this case it is an entropy sharing daemon that uses the EGD protocol.

### What is this repository for? ###

* Share /dev/hwrng or /var/run/rtl_entropy.fifo or other RNG source to EGD compatible clients (ekeyd-egd-linux).
* Setup a service like [http://hundun.ae7.st/](http://hundun.ae7.st/) using any RNG source.

### How do I get set up? ###
* RNG source required. RTL-SDR or local hardware RNG.
* [rtl-entropy](https://github.com/pwarren/rtl-entropy) for SDR dongles.  
* bcm2708_rng module for Raspberry Pi.
* Set UID and GID numbers as needed
* Set port to unused port (EGD client default: 8888)
* sudo python egd.py start