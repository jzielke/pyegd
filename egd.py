#!/usr/bin/env python

import socket
import thread
import struct
import os
import logging
import logging.handlers
import sys, time
from daemon import Daemon

host = '127.0.0.1'
port = 12345
#dev  = '/dev/hwrng'
dev  = '/var/run/rtl_entropy.fifo'
dir  = '/var/log/egd/'
log  = dir + 'egd.log'

user  = 65534	# nobody
group = 65534	# nogroup

# create logging directory
try:
  os.makedirs(dir)
  os.chown(dir, user, group)
except OSError:
  if not os.path.isdir(dir):
    raise

log_handler = logging.handlers.WatchedFileHandler(log)
log_handler.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.addHandler(log_handler)

POOLSIZE      = 4096
MIN_ENTROPY   = POOLSIZE * 0.25
MAX_ENTROPY   = POOLSIZE

pool = ''

def fill_pool():
    global pool
    f = open(dev, 'rb')
    while len(pool) < MAX_ENTROPY:
      pool += f.read(512)
    f.close()

def client(c,addr):
    global pool
    while 1:
        data = c.recv(1024)
        if not data: break
        logging.debug('%s recv: %s', repr(addr[0]), repr(data))
        while len(data) > 0:
            if 0 == ord(data[0]):
                # get pool size
                c.send(struct.pack('>L', len(pool)))
                data = data[1:]
            elif 1 == ord(data[0]):
                # get non-blocking entropy
                if len(data) < 2:
                    continue
                count = ord(data[1])
                if count > len(pool):
                    count = len(pool)
                c.send(struct.pack('B', count) + pool[0:count])
                pool = pool[count:]
                data = data[2:]
                if len(pool) < MIN_ENTROPY:
                    fill_pool()
            elif 2 == ord(data[0]):
                # get blocking entropy
                if len(data) < 2:
                    continue
                wanted = ord(data[1])
                count = wanted
                while wanted > 0:
                    if count > len(pool):
                        count = len(pool)
                    c.send(pool[0:count])
                    pool = pool[count:]
                    wanted -= count
                    if wanted > 0:
                        fill_pool()
                data = data[2:]
                if len(pool) < MIN_ENTROPY:
                    fill_pool()
            elif 3 == ord(data[0]):
                # receive entropy from client
                # discard data
                if len(data) < 4:
                    continue
                (bits, bytes) = struct.unpack('>HB', data[1:4])
                if len(data) < 4 + bytes:
                    continue
                #add_entropy(data[4:bytes+4])
                data = data[bytes+4:]
            elif 4 == ord(data[0]):
                # get PID
                #pid = os.getpid()
                pid = -1
                c.send(struct.pack('B', len(str(pid))) + str(pid))
                data = data[1:]
            else:
                logging.warning('%s Unknown command %s', addr[0], repr(data[0]))
                break
    c.close()
    logging.info('%s Connection closed', addr[0])

class MyDaemon(Daemon):
    def run(self):
        s = socket.socket()
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((host, port))
        s.listen(5)
        os.setegid(group)
        os.seteuid(user)
        logging.info('pyEGD gateway listening on port %s', port)
        fill_pool()
        while 1:
            c, addr = s.accept()
            logging.info('%s New Connetion', addr[0])
            thread.start_new_thread(client, (c, addr))

if __name__=='__main__':
    daemon = MyDaemon('/var/run/egd.pid')
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)